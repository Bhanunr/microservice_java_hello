FROM openjdk:18-ea-11-jdk-alpine
#ARG JAR_FILE=build/libs/*.jar
COPY build/libs/*.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
